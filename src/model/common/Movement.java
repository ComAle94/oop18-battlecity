package model.common;

/**
 * The movement of a game entity. Representing a vector of movement.
 *
 */
public interface Movement {
    /**
     * Set current movement.
     */
    void setMovement(double xMovement, double yMovement);

    /**
     * 
     * @return the current x offset of the movement
     */
    double getXMovement();

    /**
     * 
     * @return the current y offset of the movement
     */

    double getYMovement();

    /**
     * Multiply actual movement for a given speed.
     * 
     * @param speed the variable speed to set at the movement.
     */
    void mul(double speed);

}
