package model.common;

/**
 * Represent an object dimension in the world.
 */
public interface Dimension {
    /**
     * 
     * @return the height of the object
     */
    double getHeight();

    /**
     * 
     * @return the width of the object
     */

    double getWidth();

}
