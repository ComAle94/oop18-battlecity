package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import model.command.Direction;
import model.common.Movement;
import model.common.MovementImpl;
import model.common.Position;
import model.entities.Bullet;
import model.entities.BulletImpl;
import model.entities.Tank;
import model.entities.tankcomponents.BulletComponent;
import model.entities.tankcomponents.PowerBulletComponent;
import model.entities.tankcomponents.SpeedBulletComponent;

/**
 * The engine that manage shooting form the tank in the world. It manage the
 * speed of the bullet, the number of bullet of every tank can shoot
 * simultaneously and the type of bullet that the tank can shoot That is managed
 * by the Components attached to the tank.
 *
 */
public final class BulletEngine {
    private static final double DEFAULT_TANK_TO_BULLET_OFFSET = 2.6;
    private static final double DEFAULT_BULLET_DELTA_MOVEMENT = 0.2;
    private static final double DEFAULT_SPEED_BULLET_MOLTIPLICATOR = 2;
    private final Map<Tank, List<Bullet>> currentBullets;
    private final World world;

    /**
     * Default constructor.
     * 
     * @param attachedWorld the world where notify the bullet creation.
     */
    public BulletEngine(final World attachedWorld) {
        super();
        this.currentBullets = new HashMap<Tank, List<Bullet>>();
        this.world = attachedWorld;
    }

    /**
     * Add a new bullet to the bullet manager . This is correlated to a tank to
     * manage the number of bullet that the tank is currently shooting.
     * 
     * @param attacchedTank the tank related at the new bullet.
     */
    public void addBullet(final Tank attacchedTank) {
        if (!currentBullets.containsKey(attacchedTank)) {
            this.currentBullets.put(attacchedTank, new ArrayList<>());
        }
        if (this.currentBullets.get(attacchedTank).size() < attacchedTank.getComponents().stream()
                .filter(e -> e instanceof BulletComponent).count()) {
            this.world.addBullet(this.getBullet(attacchedTank));
        }
    }

    /**
     * Remove a bullet from the bullet collection if the related tank is present.
     * 
     * @param bullet the bullet to remove.
     */
    public void removeBullet(final Bullet bullet) {
        if (currentBullets.containsKey(bullet.getAttachedTank())) {
            currentBullets.get(bullet.getAttachedTank()).remove(bullet);
        }

    }

    /**
     * Method that create a bullet according to the tank components attached.
     * 
     * @param attacchedTank
     * @return a new bullet with the correct characteristic
     */
    private Bullet getBullet(final Tank attacchedTank) {
        final Position position = this.getBulletPosition(attacchedTank);
        final Movement movement = this.getBulletMovement(attacchedTank.getDirection());
        if (attacchedTank.getComponents().stream().filter(e -> e instanceof SpeedBulletComponent).count() > 0) {
            movement.mul(DEFAULT_SPEED_BULLET_MOLTIPLICATOR); // Se ha uno speed component aumento la velocità del
                                                              // bullet
        }
        final Bullet bullet = new BulletImpl(position, movement,
                attacchedTank.getComponents().stream().filter(e -> e instanceof PowerBulletComponent).count() > 0
                        // Se il tank ha un componente del tipo powebullet allora imposto il bullet come
                        // perforante
                        ? Bullet.Power.ARMOR_PIERCING
                        : Bullet.Power.NORMAL,
                attacchedTank, world);
        if (attacchedTank.getDirection().isPresent()) {
            bullet.setDirection(attacchedTank.getDirection().get());
        }

        this.currentBullets.get(attacchedTank).add(bullet);
        return bullet;
    }

    /**
     * Method that manage the correct movement of a bullet depending on the tank
     * direction.
     * 
     * @param sourceDirection the direction of the source tank
     * @return the movement to setup at the bullet
     */
    private Movement getBulletMovement(final Optional<Direction> sourceDirection) {
        if (sourceDirection.isPresent()) {
            switch (sourceDirection.get()) {
            case UP:
                return new MovementImpl(0, -DEFAULT_BULLET_DELTA_MOVEMENT);
            case DOWN:
                return new MovementImpl(0, DEFAULT_BULLET_DELTA_MOVEMENT);

            case LEFT:
                return new MovementImpl(-DEFAULT_BULLET_DELTA_MOVEMENT, 0);
            case RIGHT:
                return new MovementImpl(DEFAULT_BULLET_DELTA_MOVEMENT, 0);

            default:

            }
        }
        return new MovementImpl(0, -DEFAULT_BULLET_DELTA_MOVEMENT);

    }

    /**
     * A method that select the correct point of start of the bullet.
     * 
     * @param sourceTank the source tank to extract position and direction.
     * @return the correct position to setup at the bullet
     */
    private Position getBulletPosition(final Tank sourceTank) {
        final Position bulletPosition = sourceTank.getActualPosition();
        double deltaX = 0;
        double deltaY = 0;
        if (sourceTank.getDirection().isPresent()) {
            switch (sourceTank.getDirection().get()) {
            case UP:
                deltaX = sourceTank.getDimension().getWidth() / DEFAULT_TANK_TO_BULLET_OFFSET;
                break;
            case DOWN:
                deltaX = sourceTank.getDimension().getWidth() / DEFAULT_TANK_TO_BULLET_OFFSET;
                deltaY = sourceTank.getDimension().getHeight();
                break;
            case LEFT:
                deltaY = sourceTank.getDimension().getHeight() / DEFAULT_TANK_TO_BULLET_OFFSET;
                break;
            case RIGHT:
                deltaX = sourceTank.getDimension().getWidth();
                deltaY = sourceTank.getDimension().getHeight() / DEFAULT_TANK_TO_BULLET_OFFSET;
                break;
            default:
                break;
            }
        } else {
            deltaX = sourceTank.getDimension().getHeight() / DEFAULT_TANK_TO_BULLET_OFFSET;
        }

        return bulletPosition.getSumPosition(deltaX, deltaY);
    }

}
