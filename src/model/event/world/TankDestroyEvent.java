package model.event.world;

import model.entities.Bullet;
import model.entities.Tank;

/**
 * Represent the destruction of a tank.
 *
 */
public final class TankDestroyEvent extends WorldEvent {

    private final Tank hitTank;
    private final Bullet bullet;

    /**
     * 
     * @param hitTank      the tank that is hitted
     * @param sourceBullet the bullet who hit the tank.
     */
    public TankDestroyEvent(final Tank hitTank, final Bullet sourceBullet) {
        this.bullet = sourceBullet;
        this.hitTank = hitTank;
    }

    /**
     * 
     * @return the tank who is hitted
     */
    public Tank getHitTank() {
        return this.hitTank;
    }

    /**
     * 
     * @return the bullet who hit the tank.
     */
    public Bullet getSourceBullet() {
        return bullet;

    }

}
