package model.command;
/**
 * Command that move the tank down.
 */
public final class MoveDownCommand extends AbstractMovementCommand implements Command {
    /**
     * Default constructor that setup the right direction.
     */
    public MoveDownCommand() {
        super(Direction.DOWN);

    }

}
