package model.entities;

import enums.Sprite;
import model.World;
import model.collision.CollisionUtils;
import model.common.Dimension;
import model.common.DimensionImpl;
import model.common.Movement;
import model.common.Position;
import model.event.world.BlockDestroyEvent;
import model.event.world.BulletDestroyEvent;
import model.event.world.TankDestroyEvent;

/**
 * Implementation of the {@link Bullet}.
 *
 */
public final class BulletImpl extends AbstractGameEntity implements Bullet {

    private static final Dimension DEFAULT_BULLET_DIMENSION = new DimensionImpl(0.4, 0.4);
    private final Power power;
    private final Tank attachedTank;
    private final World world;

    /**
     * Create a new bullet with given parameter. i now that give to an object the
     * conception of the world is so ugly and unsafe but is fast to implement. the
     * project delivery is to near to refactor.
     * 
     * @param position      the position of the bullet
     * @param movement      the movement of the bullet
     * @param power         the power of the bullet
     * @param attacchedTank the tank who shoot the bullet.
     * @param world         the world to get all the world object.
     */
    public BulletImpl(final Position position, final Movement movement, final Power power, final Tank attacchedTank,
            final World world) {
        super(Sprite.BULLET, position, movement, DEFAULT_BULLET_DIMENSION);
        this.power = power;
        this.attachedTank = attacchedTank;
        this.world = world;
    }

    /**
     * That ugly method implements mechanics of the collision with the entities in
     * the world. I now is very ugly but the project delivery is to near. All
     * collision management system is to refactor.
     * 
     */
    @Override
    public void updateState() {
        world.getWorldEntity().stream().forEach(e -> {
            if (!this.getAttachedTank().equals(e) && CollisionUtils.collide(this, e)) {
                if (e instanceof Block) {
                    if (((Block) e).getType().equals(Block.Type.WALL)
                            || ((Block) e).getType().equals(Block.Type.BASE)) { // Elimino il muro di mattoni o la base
                                                                                // se vengono
                                                                                // colpiti
                        this.world.notifyEvent(new BlockDestroyEvent((Block) e));
                        this.world.notifyEvent(new BulletDestroyEvent(this));
                    }
                    if (((Block) e).getType().equals(Block.Type.IRON)) { // Posso rompere l'acciao se il proiettile è
                                                                         // perforante
                        if (this.getPower().equals(Bullet.Power.ARMOR_PIERCING)) {
                            this.world.notifyEvent(new BlockDestroyEvent((Block) e));
                        }
                        this.world.notifyEvent(new BulletDestroyEvent(this));
                    }

                }
                if (e instanceof Tank) { // Elimino il nemico e il proiettile che lo ha colpito
                    this.world.notifyEvent(new TankDestroyEvent((Tank) e, this));
                    this.world.notifyEvent(new BulletDestroyEvent(this));
                }
                if (e instanceof Bullet) { // Elimino tutti e due i proiettili
                    this.world.notifyEvent(new BulletDestroyEvent(this));
                    this.world.notifyEvent(new BulletDestroyEvent((Bullet) e));
                }

            }
        });
        if (CollisionUtils.collideWithBorder(this)) {
            this.world.notifyEvent(new BulletDestroyEvent(this));
        }
        super.updateState();
    }

    @Override
    public Power getPower() {
        return this.power;
    }

    @Override
    public Tank getAttachedTank() {
        return this.attachedTank;
    }

}
