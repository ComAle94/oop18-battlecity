package model.entities.tankcomponents;

/**
 * Interface that represent a component who need to be used.
 *
 */
public interface ActiveTankComponent extends TankComponent {
    /**
     * Do some mechanics.
     */
    void useComponent();
}
