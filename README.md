### What is this repository for? ###

The purpose of this project is to emulate the classic arcade game "Battle City" by Namco Ltd. using Java programming language.

The stages start with differing numbers and tipe of blocks and 20 enemies of different type to defeat. You win when all the enemies are destroyed.
You lose when your base is destroyed or when you lose all the lives.

## Story ###
You are the last member of your army's elite tank commanders, with no choice but to defend your fortress against an entire army of enemy tanks bent on your base's destruction.
Break holes through the walls and situate yourself strategically to remove the enemy as quickly as possible, but stay out of their line of fire, and don't let them shoot your base down.

### Gameplay summary ###
 * You start out at the bottom of the screen next to your base. Enemy tanks will appear from one of three positions at the top of the screen.
 
 * In each stage, there are 20 tanks in total you must defeat in order to advance to the next stage.

 * You can fire in four directions. Most tanks only require one hit to destroy them.
 
 * If one enemy bullet hits you, you lose one life. If your base is ever hit by a bullet, the game is automatically over.

 * Bullets can destroy walls, whether they are fired by you or the enemy.

### Modes ###
There are three game modes:

* One Player: you are alone against all the 20 enemies of every stage.
	
* Two Player: you and your companion tank are together against all the 20 enemies of every stage.

* Construction: not implemented yet.

### How do I get set up? ###
1. Install the Java Runtime environment [here](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
2. Download BattleCity.jar in the *Downloads* section
3. Doubleclick to launch application
4. Enjoy!

### Makers ###
* Alessio Comandini
* Marco Maltoni

This project was realized for the Object Oriented Programming course in Computer Science and Engineering department of UniBo, Cesena.